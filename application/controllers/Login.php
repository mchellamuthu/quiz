<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		parent::__construct();

	}
	public function doLogin()
	{
	
		$this->form_validation->set_rules('username','Username', 'required');
		if ($this->form_validation->run() == true) {
		

			$username = $this->input->post('username');
			$sql = $this->db->select('id,username')->where('username', $username)->get('users');
			$row= $sql->row();
			if (empty($row)) {
				$this->db->insert('users',[
					'username'=>$username
				]);
				$sql = $this->db->select('id,username')->where('username', $username)->get('users');
				$row = $sql->row();
				$this->session->set_userdata(['id'=>$row->id,'username'=>$row->username]);
			}else {
				$this->session->set_userdata(['id' => $row->id,'username' => $row->username]);
			}
			redirect('/questions');
		}
		$this->session->set_flashdata('error', validation_errors());
		redirect('/');

	}
}
