<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Questions extends CI_Controller
{

	/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		parent::__construct();



		if (!$this->session->has_userdata('username')) {
			redirect('/');
		}
	}

	public function index()
	{
		$data['content'] = $this->load->view('questions', '', true);
		return $this->load->view('layout', $data);
	}

	public function loadQuestion()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$user_id = $this->session->userdata('id');
		$this->db->select('*')->from('questions');
		$this->db->where('`id` NOT IN (SELECT `question_id` FROM `scores` WHERE `user_id`= '.$user_id.')', NULL, FALSE);
		$sql = $this->db->order_by('RAND()')->limit(1)->get();
		if (empty($sql->row())) {
			echo json_encode([
				'question' => null,
				'answers' => null,
			]);
		}else{

			$question = $sql->row();
			$options = $this->db->where('question_id', $question->id)->select('id,options')->get('answers');
			echo json_encode([
				'question' => $question,
				'answers' => $options->result(),
			]);
		}
	}

	public function doSkip()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$question_id = $this->input->post('question');
		$user_id = $this->session->userdata('id');
		$this->db->insert(
			'scores',
			[
				'user_id' => $user_id,
				'question_id' => $question_id,
				'answer_type' => 'SKIP'
			]
		);
		echo json_encode(['status'=>'success']);
	}
	public function doAnswer()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$question_id = $this->input->post('question');
		$option_id = $this->input->post('option');
		$sql = $this->db->where('id', $option_id)->limit(1)->get('answers');
		$answer = $sql->row();
		
		$answer_type = $answer->is_answer==1 ? 'RIGHT' : 'WRONG' ;
		$user_id = $this->session->userdata('id');
		$this->db->insert(
			'scores',
			[
				'user_id' => $user_id,
				'question_id' => $question_id,
				'answer_type' => $answer_type
			]
		);
		echo json_encode(['status' => 'success']);

	}
	public function doResults()
	{
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$user_id = $this->session->userdata('id');
		$correct = $this->db->where('answer_type','RIGHT')->from('scores')->count_all_results();
		$wrong = $this->db->where('answer_type','WRONG')->from('scores')->count_all_results();
		$skip = $this->db->where('answer_type','SKIP')->from('scores')->count_all_results();
		echo json_encode(['correct'=> $correct,'wrong'=>$wrong,'skip'=>$skip]);

	}
}
