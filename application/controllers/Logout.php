<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		parent::__construct();

	}
	public function doLogout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}
