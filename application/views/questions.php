<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="row justify-content-md-center py-5">
	<div class="col-md-6">
		<div class="card shadow">
			<div class="card-header">
				<p id="answerTile"></p>
				<input type="hidden" name="question" id="question" value="">
			</div>
			<div class="card-body">
				<div id="pr_result"></div>

			</div>
			<div class="card-footer ">
				<button class="btn btn-danger " onclick="doSkip()" id="skip">SKIP</button>
				<button class="btn btn-success pull-right" onclick="doAnswer()" style="float: right;" id="skip">NEXT</button>
			</div>
		</div>
	</div>
</div>

<script>
	function loadQst() {
		$.ajax({
			type: "POST",
			url: '<?php echo base_url() . '/questions/loadQuestion' ?>',
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			cache: false,

			success: function(data) {
				if (data.question != null) {
					$('#answerTile').html(data.question.title);
					$('#question').attr('value', data.question.id);
					$('.card-body').html('<div id="pr_result"></div>');
					var options = data.answers;

					$.each(options, function(index) {
						$("#pr_result").append('<p><input type="radio" id="' + options[index].id + '" name="answer" value="' + options[index].id + '"> <label for="' + options[index].id + '">' + options[index].options + '</label> </p>');
					});
				} else {
					loadResults();
				}
			}
		});

	}

	function loadResults() {
		$.ajax({
			type: "POST",
			url: '<?php echo base_url() . '/questions/doResults' ?>',
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			cache: false,

			success: function(data) {
				$('#answerTile').html('Results');
				$('.card-body').html('<div id="pr_result"></div>');
				$("#pr_result").append('<p> Correct Answers : (' + data.correct + ' )</p>');
				$("#pr_result").append('<p> Wrong Answers : (' + data.wrong + ' )</p>');
				$("#pr_result").append('<p> Skipped Answers : (' + data.skip + ' )</p>');
				$('.card-footer').html('');

			}
		});
	}
	// this is the id of the form
	$(document).ready(function() {
		loadQst();
	});
</script>


<script>
	function doSkip() {
		var question = $('#question').val();
		$.ajax({
			type: "POST",
			url: '<?php echo base_url() . '/questions/doSkip' ?>',
			data: {
				question: question
			},
			success: function(data) {
				loadQst();
			}
		});
	}

	function doAnswer() {

		var question = $('#question').val();
		var answer = $('input[name="answer"]:checked').val();
		if ($('input:radio').filter(':checked').length < 1) {
			swal("Error!", "Please Check at least one answer!", "error");
			return false;
		} else {
			$.ajax({
				traditional: true,

				type: "POST",
				url: '<?php echo base_url() . '/questions/doAnswer' ?>',
				data: {
					question: question,
					option: answer
				},
				cache: false,
				success: function(data) {
					loadQst();
				}
			});
		}

	}
</script>
