<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="row justify-content-md-center py-4">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				Enter your username
			</div>
			<div class="card-body">
				<form action="/login/doLogin" method="post" id="storeName">
					<div class="form-group">
						<input type="text" name="username" class="form-control <?php if (!empty($error)) {
																					echo 'is-invalid';
																				}  ?>" id="username" placeholder="Enter your username">
					</div>
					<div class="form-group mt-2">
						<button type="submit" class="btn btn-success">Start</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- <script>
	// this is the id of the form
	$("#storeName").submit(function(e) {

		e.preventDefault(); // avoid to execute the actual submit of the form.

		var form = $(this);
		var url = form.attr('action');

		$.ajax({
			type: "POST",
			url: url,
			data: form.serialize(), // serializes the form's elements.
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(data) {

			}
		});


	});
</script> -->
