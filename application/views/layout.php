<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	<script src="/assets/js/jquery.blockUI.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


	<title>Welcome</title>
	<script>
		$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	</script>
</head>

<body>
	<div class="container">
		<?php if ($this->session->has_userdata('username')) { ?>
			<div class="row py-4">
				<div class="col-md-12">
					<p class="">
					
					<a href="/logout/doLogout" class="btn btn-sm btn-danger" style="float:right">
					
					Logout</a></p>
				</div>
			</div>
		<?php } ?>
		<?php echo $content; ?>

	</div>
	<!-- Option 1: Bootstrap Bundle with Popper -->

</body>

</html>
